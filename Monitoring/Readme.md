1. Adding The VM dashboard

The ordinary openshift dashboard that we can easily access throught the adresse `127.0.0.1:8443` does not allow a global vision of all the deployed applications if we have applications isolated within VMs.
In order to have a more gloable dashboard that also displays the VMs hosted in the openshift cluster, here are the steps to follow:

Access the predefined project `kube-system`

```
oc project kube-system
oc create serviceaccount console -n kube-system
oc create clusterrolebinding console --clusterrole=cluster-admin --serviceaccount=kube-system:console -n kube-system
```

Get the token that will appear once you enter the following command
`oc get serviceaccount console --namespace=kube-system -o jsonpath='{.secrets[0].name}'`

This token will be inserted at the end of the yaml file `console-deployment.yaml` that is joined in the monitoring repository.

```
secretKeyRef:
 name: <token>
 key: token
```

The two yaml files will be added to openshift in the kube-system namespace by the following commands: 


```
oc create -f console-deployment.yaml -n kube-system
oc create -f console-np-service.yaml -n kube-system
```

The application console deployment should be then up and running on the kube-system project. And you can access the created dashboard through the address `127.0.0.1:30036`.




