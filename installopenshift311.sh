####################################################################
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world
echo "=============================> docker installed"
####################################################################
sudo rm -rf /var/lib/docker 
sudo touch /etc/docker/daemon.json
cat >> /etc/docker/daemon.json << EOF
{
    "insecure-registries": [
     "172.30.0.0/16"
   ]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
echo "=============================> docker issues solved"
#################################
cat >> /etc/docker/daemon.json << EOF
{
   "insecure-registries": [
     "172.30.0.0/16"
   ]
   "graph": "/mnt/docker-data",
    "storage-driver": "overlay"
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
docker network inspect -f "{{range .IPAM.Config }}{{ .Subnet }}{{end}}" bridge
sysctl net.ipv4.ip_forward
apt install firewalld
firewall-cmd --permanent --new-zone dockerc
firewall-cmd --permanent --zone dockerc --add-source 172.17.0.0/16
firewall-cmd --permanent --zone dockerc --add-port 8443/tcp
firewall-cmd --permanent --zone dockerc --add-port 53/udp
firewall-cmd --permanent --zone dockerc --add-port 8053/udp
firewall-cmd --reload
echo "=============================>host ready"
###############################
wget https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
tar -zvxf openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
cd openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit
sudo cp oc /usr/local/bin/
echo "=============================> openshiftinstalled"
###############################
mkdir projects_port_files