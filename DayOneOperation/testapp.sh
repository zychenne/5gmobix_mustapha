sudo git clone https://github.com/dockersamples/node-bulletin-board
cd node-bulletin-board/bulletin-board-app
sudo docker build --tag bulletinboard:1.0 .

echo "=============================> App Downloaded and built"
#################################################

sh new-project.sh test
oc new-app bulletinboard:1.0 --name=builletinboardapp
oc status

echo "=============================> App running on openshift"
#################################################
oc expose dc/builletinboardapp --type=NodePort --name=bullettinboard
oc get service
oc get service | grep NodePort > projects_port_files/test_file_port

echo "=============================> App exposed on openshift"
#################################################
