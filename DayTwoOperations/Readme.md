# Basic Application Management Guide
These steps will help you perform basic management tasks to handel your projects and application in Openshift through the CLI.

## Create/Delete projects

### Create projects
Before creating an application it is important to create a project (a namespace) that will host multiple applications that are related to each other.
The steps to create the actual project is straightforward and demande one and only command. However for good practices, it is efficient to create a file for each project in which application's ports exposure will be stored.

Starting of course by getting your cluster up and running in the same repository in which you have created. Precisely where the folder `openshift.local.cluster` exists.

```
cd home/mec/AKKA_MEC/etsimec
sudo su
oc cluster up
oc new-project <project_name>
touch <project_name>_app_ports
echo "NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE" >> <project_name>_app_ports
```

If, however, your project already exists, make sure you are making modifications into your poject and not another one, before creating or configuring, applications. using:
`oc project <project_name>`

### Delete project
To delete your project, it will also be useful to delete the file you have created for the ports related to this project.

```
oc delete <project_name>
rm <project_name>_app_ports
```

## Create/Delete applications

### Create applications

To create an application on Openshift you can use different strategies


1. Creating an Application From Source Code
2. Creating an Application From an Image
3. Creating an Application From a Template
4. Further Modifying Application Creation

In our case we will be focusing on the second strategy having an image built localy.
It is important to tag the image as "1.0" for example and try toa void "latest" tags

`oc new-app <image_name>:<image_tag> --name=<applciation_name>`

To access your application, later on , it is important to expose its service and to add the exposure port to the previously created port file associated to the project.

`oc expose dc/<applciation_name> --type=NodePort --name=<applciation_name>`
`oc get service`
`oc get service | grep NodePort > <project_name>_app_ports`

### Delete applications

Creating and working on an application may generate multiple variables. Thus, depending on what do you need to delete you can use one of the commands belows:

1. Resource objects can be deleted using the oc delete command from the command line. You can delete a single resource object by name, or delete a set of resource objects by specifying a label selector. For example:

```
oc get all --selector app=<test_app> -o name

deploymentconfigs/<test_app>
routes/<test_app>
pods/<test_app>-1-wgzdb
replicationcontrollers/<test_app>-1
services/<test_app>
```
If you are okay with deleting these options you can move forward:

`oc delete all --selector app=<test_app>`

Note that `all` matches on a subset of all resource object types that exist. It targets the core resource objects that would be created for a build and deployment. It will not include resource objects such as persistent volume claims (pvc), config maps (configmap), secrets (secret), and others.
You will either need to delete these resource objects separately, or if they also have been labelled with the app tag, list the resource object types along with all.

`oc delete all,configmap,pvc,serviceaccount,rolebinding --selector app=<test_app>`

## Adding persistent volume to a containerized aaplication

Oepnshift cluster comes with approximitevly 100 persistent volumes (pv) created and ready to be used. It is important to create a persistent volume claim (pvc) throughout the web dashboard that will automatically be associated to one of the created pv.
Once the pvc created, go to the overview application, the last running pod and add the persistent storage. A new deployment will be automatically trigered to take into consideration the new modifications.



